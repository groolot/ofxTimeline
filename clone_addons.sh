#!/bin/bash

# Dependencies for ofxTimeline

cd ../

if [ -z $1 ]; then
    PREFIX="git clone https://github.com/"
else
    PREFIX="git clone git@github.com:"
fi

${PREFIX}groolot/ofxTimecode.git

${PREFIX}groolot/ofxTween.git

${PREFIX}groolot/ofxMSATimer.git

${PREFIX}groolot/ofxTextInputField.git

${PREFIX}groolot/ofxRange.git
